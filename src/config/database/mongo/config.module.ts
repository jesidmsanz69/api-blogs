import { Module } from '@nestjs/common';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: () => ({
        uri: `${process.env.MONGO_CONNECTION}://${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}/${process.env.DATABASE_NAME}`,
        user: process.env.DATABASE_USERNAME,
        pass: process.env.DATABASE_PASSWORD,
        dbName: process.env.DATABASE_NAME,
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseMongoConfigModule {}
