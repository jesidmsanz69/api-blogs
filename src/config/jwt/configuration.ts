import { registerAs } from '@nestjs/config';

export default registerAs('jwt', () => ({
  jwtSecret: process.env.JWT_ACCESS_TOKEN_SECRET || 'secret123',
  jwtExpiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME || '15m',
  jwtRefreshSecret: process.env.JWT_REFRESH_TOKEN_SECRET || 'secret1234',
  jwtRefreshExpiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRATION_TIME || '1w',
}));
