import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';

@Injectable()
export class JwtConfigService {
  constructor(private configService: ConfigService) {}

  get jwtSecret(): string {
    return this.configService.get<string>('jwt.jwtSecret');
  }
  get jwtExpiresIn(): string {
    return this.configService.get<string>('jwt.jwtExpiresIn');
  }
  get jwtRefreshSecret(): string {
    return this.configService.get<string>('jwt.jwtRefreshSecret');
  }
  get jwtRefreshExpiresIn(): string {
    return this.configService.get<string>('jwt.jwtRefreshExpiresIn');
  }
}
