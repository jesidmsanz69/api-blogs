import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class ImageDto {
  @IsString()
  @ApiProperty()
  @IsNotEmpty()
  readonly url?: string;

  @IsString()
  @ApiProperty()
  @IsNotEmpty()
  readonly fileId?: string;

  @IsString()
  @ApiProperty()
  @IsNotEmpty()
  readonly filePath?: string;

  @IsNumber()
  @ApiProperty()
  @IsNotEmpty()
  readonly width?: number;

  @IsNumber()
  @ApiProperty()
  @IsNotEmpty()
  readonly height?: number;

  @IsNumber()
  @ApiProperty()
  @IsOptional()
  readonly order?: number;

  @IsNumber()
  @ApiProperty()
  @IsOptional()
  readonly rotate? = 0;
}
