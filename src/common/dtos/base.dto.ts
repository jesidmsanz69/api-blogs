import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class BaseDto {
  @IsString()
  @ApiProperty()
  @IsNotEmpty()
  readonly Name: string;
}
