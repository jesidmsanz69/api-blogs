import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
@Schema()
export class BaseContactInfo {
  @Prop()
  Name: string;

  @Prop()
  Email?: string;

  @Prop()
  Fax?: string;

  @Prop()
  Phone1?: string;

  @Prop()
  Phone2?: string;
}

export const BaseContactInfoSchema =
  SchemaFactory.createForClass(BaseContactInfo);
