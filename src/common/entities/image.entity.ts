import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ strict: false })
export class Image {
  @Prop() url?: string;
  @Prop() fileId?: string;
  @Prop() filePath?: string;
  @Prop() width?: number;
  @Prop() height?: number;
  @Prop() order?: number;
  @Prop() rotate?: number;
}

export const ImageSchema = SchemaFactory.createForClass(Image);
