import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Base {
  @Prop({ required: true })
  Name: string;
}

export const BaseSchema = SchemaFactory.createForClass(Base);
