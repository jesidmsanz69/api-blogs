import {
  ArgumentMetadata,
  Injectable,
  PipeTransform,
  BadRequestException,
} from '@nestjs/common';

@Injectable()
export class IsNumberPipe implements PipeTransform {
  transform(value: string, metadata: ArgumentMetadata) {
    const pattern = new RegExp('^[0-9]+$', 'i');
    if (!pattern.test(value) || value !== 'any') {
      throw new BadRequestException(`${value} not is number`);
    }
    return value;
  }
}
