import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { UpdateClientDto } from '../dtos/update-client.dto';
import { Client, ClientDocument } from '../entities/client.entity';

export class ClientsDao {
  constructor(
    @InjectModel(Client.name)
    private readonly clientsModel: Model<ClientDocument>,
  ) {}

  create(client: Client) {
    const newModel = new this.clientsModel(client);
    const result = newModel.save();
    return result;
  }

  findAll() {
    return this.clientsModel.find().exec();
  }

  findAllActive() {
    return this.clientsModel.find({ Active: true }).exec();
  }

  findByCode(Code: string) {
    return this.clientsModel.findOne({ Code }).exec();
  }

  findByEmail(Email: string) {
    return this.clientsModel.findOne({ Email }).exec();
  }

  findById(id: string) {
    return this.clientsModel.findById(id).exec();
  }

  updateById(_id: string, changes: UpdateClientDto) {
    return this.clientsModel
      .findByIdAndUpdate(_id, { $set: changes }, { new: true })
      .exec();
  }

  updateByCode(Code: string, changes: UpdateClientDto) {
    return this.clientsModel
      .findOneAndUpdate({ Code }, { $set: changes }, { new: true })
      .exec();
  }

  removeById(id: string) {
    return this.clientsModel.findByIdAndDelete(id).exec();
  }

  removeByCode(Code: string) {
    return this.clientsModel.findOneAndDelete({ Code }).exec();
  }
}
