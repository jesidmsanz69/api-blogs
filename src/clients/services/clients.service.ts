import { Injectable, NotFoundException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

import { ClientsDao } from '../daos/clients.dao';
import { CreateClientDto } from '../dtos/create-client.dto';
import { UpdateClientDto } from '../dtos/update-client.dto';

@Injectable()
export class ClientsService {
  constructor(private readonly clientDao: ClientsDao) {}
  create(createClientDto: CreateClientDto) {
    return new Promise(async (resolve, reject) => {
      try {
        const hashPassword = await bcrypt.hash(createClientDto.Password, 10);
        const result = await this.clientDao.create({
          ...createClientDto,
          Password: hashPassword,
        });

        const { Password, ...rta } = result.toJSON();
        resolve({ error: '', body: rta });
      } catch (error) {
        reject({ error: error, body: '' });
      }
    });
  }

  findAll() {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.clientDao.findAll();
        resolve({ error: '', body: result });
      } catch (error) {
        reject({ error: error, body: '' });
      }
    });
  }

  findAllActive() {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.clientDao.findAllActive();
        resolve({ error: '', body: result });
      } catch (error) {
        reject({ error: error, body: '' });
      }
    });
  }

  findByCode(code: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.clientDao.findByCode(code);
        if (!result) {
          const error = new NotFoundException(`Client ${code} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        resolve({ error: '', body: result });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }
  findByEmail(email: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.clientDao.findByEmail(email);
        if (!result) {
          const error = new NotFoundException(`Client ${email} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        resolve({ error: '', body: result });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }

  findById(id: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.clientDao.findById(id);
        if (!result) {
          const error = new NotFoundException(`Client ${id} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        resolve({ error: '', body: result });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }

  updateById(id: string, updateClientDto: UpdateClientDto) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.clientDao.updateById(id, updateClientDto);
        if (!result) {
          const error = new NotFoundException(`Client ${id} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        const { Password, ...rta } = result.toJSON();
        resolve({ error: '', body: rta });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }

  updateByCode(bcode: string, updateClientDto: UpdateClientDto) {
    return new Promise(async (resolve, reject) => {
      try {
        const clientExist = await this.clientDao.findByCode(bcode);

        if (!clientExist) {
          const error = new NotFoundException(`Client ${bcode} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        const result = await this.clientDao.updateByCode(
          bcode,
          updateClientDto,
        );
        const { Password, ...rta } = result.toJSON();
        resolve({ error: '', body: rta });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }
  removeById(id: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.clientDao.removeById(id);
        if (!result) {
          const error = new NotFoundException(`Client ${id} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        resolve({ error: '', body: result });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }

  removeByCode(bcode: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const clientExist = await this.clientDao.findByCode(bcode);

        if (!clientExist) {
          const error = new NotFoundException(`Client ${bcode} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        const result = await this.clientDao.removeByCode(bcode);
        resolve({ error: '', body: result });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }
}
