import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ClientsService } from '../services/clients.service';
import { CreateClientDto } from '../dtos/create-client.dto';
import { UpdateClientDto } from '../dtos/update-client.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('clients')
@Controller('clients')
export class ClientsController {
  constructor(private readonly clientsService: ClientsService) {}

  @Post()
  create(@Body() createClientDto: CreateClientDto) {
    return this.clientsService.create(createClientDto);
  }

  @Get()
  findAll() {
    return this.clientsService.findAll();
  }

  @Get('active')
  findAllActive() {
    return this.clientsService.findAllActive();
  }

  @Get(':id')
  findById(@Param('id') id: string) {
    return this.clientsService.findById(id);
  }

  @Get('code/:code')
  findByCode(@Param('code') code: string) {
    return this.clientsService.findByCode(code);
  }

  @Get('email/:email')
  findByEmail(@Param('email') email: string) {
    return this.clientsService.findByEmail(email);
  }

  @Patch(':id')
  updateById(
    @Param('id') id: string,
    @Body() updateClientDto: UpdateClientDto,
  ) {
    return this.clientsService.updateById(id, updateClientDto);
  }

  @Patch('code/:bcode')
  updateByCode(
    @Param('bcode') bcode: string,
    @Body() updateClientDto: UpdateClientDto,
  ) {
    return this.clientsService.updateByCode(bcode, updateClientDto);
  }

  @Delete(':id')
  removeById(@Param('id') id: string) {
    return this.clientsService.removeById(id);
  }

  @Delete('code/:code')
  removeByCode(@Param('code') code: string) {
    return this.clientsService.removeByCode(code);
  }
}
