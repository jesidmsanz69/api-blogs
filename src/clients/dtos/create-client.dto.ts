import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  Length,
} from 'class-validator';
import { Image } from 'src/common/entities/image.entity';

export class CreateClientDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly Code: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly Name: string;

  @IsString()
  @IsEmail()
  @ApiProperty({ description: "the client' email" })
  readonly Email: string;

  @IsString()
  @IsNotEmpty()
  @Length(6)
  readonly Password: string;

  @IsNotEmpty()
  readonly Role: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  readonly Phone1: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  readonly Phone2: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  readonly CurrentHashedRefreshToken: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  readonly Url: string;


  @ApiProperty()
  @IsOptional()
  readonly Logo: Image;

  @IsString()
  @ApiProperty()
  @IsOptional()
  readonly Color: string;

  @IsBoolean()
  @Type(() => Boolean)
  readonly Active = true;
}
