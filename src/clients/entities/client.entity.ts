import { Image, ImageSchema } from './../../common/entities/image.entity';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class Client {
  @Prop({ required: true, unique: true })
  Code: string;

  @Prop({ required: true, unique: true })
  Name: string;

  @Prop({ required: true, unique: true })
  Email: string;

  @Prop({ required: true })
  Password: string;

  @Prop({ required: true })
  Role: string;

  @Prop()
  Phone1?: string;

  @Prop()
  Phone2?: string;

  @Prop()
  CurrentHashedRefreshToken?: string;

  @Prop()
  Url: string;

  @Prop({ type: ImageSchema })
  Logo?: Image;

  @Prop()
  Color?: string;

  @Prop({ type: Boolean, default: true })
  Active: boolean;
}
export type ClientDocument = Client & Document;

export const ClientSchema = SchemaFactory.createForClass(Client);
