import { AuthController } from './controllers/auth.controller';
import { AuthService } from './services/auth.service';
import { JWTStrategy } from './strategies/jwt.strategy';
import { JwtConfigModule } from 'src/config/jwt/config.module';
import { JwtConfigService } from 'src/config/jwt/config.service';
import { JwtModule } from '@nestjs/jwt';
import { LocalStrategy } from './strategies/local.strategy';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { ClientsModule } from 'src/clients/clients.module';
import { RefreshStrategy } from './strategies/refresh.strategy';

@Module({
  imports: [
    ClientsModule,
    PassportModule,
    JwtModule.registerAsync({
      imports: [JwtConfigModule],
      useFactory: async (jwtConfigService: JwtConfigService) => ({
        secret: jwtConfigService.jwtSecret,
        signOptions: {
          expiresIn: jwtConfigService.jwtExpiresIn,
        },
      }),
      inject: [JwtConfigService],
    }),
    JwtConfigModule,
  ],
  providers: [
    AuthService,
    LocalStrategy,
    JwtConfigService,
    RefreshStrategy,
    JWTStrategy,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
