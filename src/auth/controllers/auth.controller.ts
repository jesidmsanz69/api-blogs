import { Controller, Post, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../services/auth.service';
import { Client } from 'src/clients/entities/client.entity';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { LoginDto } from '../dtos/login.dto';
import { JwtPayload } from '../models/token.model';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @UseGuards(AuthGuard('local'))
  @Post('login')
  @ApiBody({
    description: 'Credentials',
    type: LoginDto,
  })
  login(@Req() req: Request) {
    const client = req.user as Client;
    return this.authService.generateJWT(client);
  }

  @UseGuards(AuthGuard('refresh'))
  @Post('refresh')
  refresh(@Req() req: Request) {
    const refreshToken = req
      ?.get('authorization')
      ?.replace('Bearer', '')
      .trim();

    const client = req.user as JwtPayload;
    return this.authService.refresh(client.sub, refreshToken);
  }
}
