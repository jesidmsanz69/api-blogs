import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from '../services/auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'Email',
      passwordField: 'Password',
    });
  }

  validate(Email: string, Password: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = await this.authService.validateClient(Email, Password);
        if (!client) {
          throw new UnauthorizedException('Not allow');
        }
        resolve(client);
      } catch (err) {
        reject(err);
      }
    });
  }
}
