import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';

import { Injectable } from '@nestjs/common';
import { JwtConfigService } from 'src/config/jwt/config.service';
import { PassportStrategy } from '@nestjs/passport';
import { PayloadToken } from '../models/token.model';

@Injectable()
export class RefreshStrategy extends PassportStrategy(Strategy, 'refresh') {
  constructor(jwtConfigService: JwtConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConfigService.jwtRefreshSecret,
    } as StrategyOptions);
  }

  validate(payload: PayloadToken) {
    return payload;
  }
}
