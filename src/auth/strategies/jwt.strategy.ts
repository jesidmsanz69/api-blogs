import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';

import { Injectable } from '@nestjs/common';
import { JwtConfigService } from 'src/config/jwt/config.service';
import { PassportStrategy } from '@nestjs/passport';
import { PayloadToken } from '../models/token.model';

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(jwtConfigService: JwtConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConfigService.jwtSecret,
    } as StrategyOptions);
  }

  validate(payload: PayloadToken) {
    return payload;
  }
}
