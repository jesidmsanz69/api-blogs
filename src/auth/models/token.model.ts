import { ClientDocument } from 'src/clients/entities/client.entity';

export interface PayloadToken {
  access_token: string;
  refresh_token: string;
  client: ClientDocument;
}

export interface JwtPayload {
  role: ClientDocument['Role'];
  sub: ClientDocument['Code'];
}
