import { ForbiddenException, Injectable, Logger } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { ClientsDao } from 'src/clients/daos/clients.dao';
import { Client, ClientDocument } from 'src/clients/entities/client.entity';
import { JwtPayload, PayloadToken } from '../models/token.model';
import { JwtConfigService } from 'src/config/jwt/config.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly clientDao: ClientsDao,
    private readonly jwtService: JwtService,
    private jwtConfigService: JwtConfigService,
  ) {}

  validateClient(Email: string, Password: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = await this.clientDao.findByEmail(Email);
        if (client) {
          const isMatch = await bcrypt.compare(Password, client.Password);
          if (client && isMatch) {
            const { Password, ...rta } = client.toJSON();
            resolve(rta);
          }
        }
        resolve(null);
      } catch (err) {
        console.log('Error', err);
        reject(err);
      }
    });
  }

  async generateJWT(client: Client) {
    const payload: JwtPayload = { role: client.Role, sub: client.Code };
    const accessToken = await this.jwtService.signAsync(payload);
    const refreshToken = await this.jwtService.signAsync(payload, {
      secret: this.jwtConfigService.jwtRefreshSecret,
      expiresIn: this.jwtConfigService.jwtRefreshExpiresIn,
    });
    const hashRefreshToken = await bcrypt.hash(refreshToken, 10);
    await this.clientDao.updateByCode(client.Code, {
      CurrentHashedRefreshToken: refreshToken,
    });
    const currentDateObj = new Date();
    const numberOfMlSeconds = currentDateObj.getTime();
    const addMlSeconds = 15 * 60000;
    const newDateObj = new Date(numberOfMlSeconds + addMlSeconds);

    return {
      access_token: accessToken,
      refresh_token: refreshToken,
      client: client as ClientDocument,
      access_token_expiry: newDateObj,
    };
  }

  async refresh(Code: string, refreshToken: string): Promise<PayloadToken> {
    return new Promise(async (resolve, reject) => {
      try {
        const client = await this.clientDao.findByCode(Code);
        if (!client || !client.CurrentHashedRefreshToken) {
          const isMatch = await bcrypt.compare(
            refreshToken,
            client.CurrentHashedRefreshToken,
          );
          console.log(
            '🚀 ~ file: auth.service.ts ~ line 64 ~ AuthService ~ returnnewPromise ~ isMatch',
            isMatch,
          );
          if (!isMatch) {
            throw new ForbiddenException('Access Denied');
          }
        }

        resolve(this.generateJWT(client));
      } catch (err) {
        console.log('Error', err);
        reject(err);
      }
    });
  }
}
