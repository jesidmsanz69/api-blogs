import { Module } from '@nestjs/common';
import { AppConfigModule } from './config/app/config.module';
import { DatabaseMongoConfigModule } from './config/database/mongo/config.module';
import { ClientsModule } from './clients/clients.module';
import { AuthModule } from './auth/auth.module';
import { ScheduleModule } from '@nestjs/schedule';
import { BlogsModule } from './blogs/blogs.module';

@Module({
  imports: [
    AppConfigModule,
    DatabaseMongoConfigModule,
    BlogsModule,
    ClientsModule,
    AuthModule,
    ScheduleModule.forRoot(),
  ],
  controllers: [],
})
export class AppModule {}
