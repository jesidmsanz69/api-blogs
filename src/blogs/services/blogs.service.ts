import { Injectable, NotFoundException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

import { BlogsDao } from '../daos/blogs.dao';
import { CreateBlogDto } from '../dtos/create-blog.dto';
import { UpdateBlogDto } from '../dtos/update-blog.dto';

@Injectable()
export class BlogsService {
  constructor(private readonly blogDao: BlogsDao) {}
  create(createBlogDto: CreateBlogDto) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.blogDao.create(createBlogDto);
        resolve({ error: '', body: result });
      } catch (error) {
        reject({ error: error, body: '' });
      }
    });
  }

  findAll() {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.blogDao.findAll();
        resolve({ error: '', body: result });
      } catch (error) {
        reject({ error: error, body: '' });
      }
    });
  }

  findAllByBClientCode(BClientCode: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.blogDao.findAllByBClientCode(BClientCode);
        resolve({ error: '', body: result });
      } catch (error) {
        reject({ error: error, body: '' });
      }
    });
  }

  findAllActive() {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.blogDao.findAllActive();
        resolve({ error: '', body: result });
      } catch (error) {
        reject({ error: error, body: '' });
      }
    });
  }

  findById(id: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.blogDao.findById(id);
        if (!result) {
          const error = new NotFoundException(`Blog ${id} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        resolve({ error: '', body: result });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }

  updateById(id: string, updateBlogDto: UpdateBlogDto) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.blogDao.updateById(id, updateBlogDto);
        if (!result) {
          const error = new NotFoundException(`Blog ${id} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        const { Password, ...rta } = result.toJSON();
        resolve({ error: '', body: rta });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }

  removeById(id: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.blogDao.removeById(id);
        if (!result) {
          const error = new NotFoundException(`Blog ${id} no found`);
          resolve({ error: error.getResponse(), body: '' });
        }
        resolve({ error: '', body: result });
      } catch (error) {
        console.log('error', error);
        reject({ error: error, body: '' });
      }
    });
  }
}
