import { Image, ImageSchema } from '../../common/entities/image.entity';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class Blog {
  @Prop({ required: true })
  readonly ClientCode: string;

  @Prop({ required: true, unique: true })
  readonly Title: string;

  @Prop({ required: true })
  readonly Body: string;

  @Prop({ type: ImageSchema })
  readonly Image?: Image;

  @Prop({ type: Boolean, default: true })
  readonly Active: boolean;
}
export type BlogDocument = Blog & Document;

export const BlogSchema = SchemaFactory.createForClass(Blog);
