import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Image } from 'src/common/entities/image.entity';

export class CreateBlogDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly ClientCode: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly Title: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly Body: string;

  @ApiProperty()
  @IsOptional()
  readonly Image?: Image;

  @IsBoolean()
  @Type(() => Boolean)
  readonly Active = true;
}
