import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BlogsService } from '../services/blogs.service';
import { CreateBlogDto } from '../dtos/create-blog.dto';
import { UpdateBlogDto } from '../dtos/update-blog.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('blogs')
@Controller('blogs')
export class BlogsController {
  constructor(private readonly blogsService: BlogsService) {}

  @Post()
  create(@Body() createBlogDto: CreateBlogDto) {
    return this.blogsService.create(createBlogDto);
  }

  @Get()
  findAll() {
    return this.blogsService.findAll();
  }

  @Get('byBClientCode/:code')
  findAllByBClientCode(@Param('code') BClientCode: string) {
    return this.blogsService.findAllByBClientCode(BClientCode);
  }

  @Get('active')
  findAllActive() {
    return this.blogsService.findAllActive();
  }

  @Get(':id')
  findById(@Param('id') id: string) {
    return this.blogsService.findById(id);
  }

  @Patch(':id')
  updateById(@Param('id') id: string, @Body() updateBlogDto: UpdateBlogDto) {
    return this.blogsService.updateById(id, updateBlogDto);
  }

  @Delete(':id')
  removeById(@Param('id') id: string) {
    return this.blogsService.removeById(id);
  }
}
