import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { UpdateBlogDto } from '../dtos/update-blog.dto';
import { Blog, BlogDocument } from '../entities/blog.entity';

export class BlogsDao {
  constructor(
    @InjectModel(Blog.name)
    private readonly blogsModel: Model<BlogDocument>,
  ) {}

  create(blog: Blog) {
    const newModel = new this.blogsModel(blog);
    const result = newModel.save();
    return result;
  }

  findAll() {
    return this.blogsModel.find().exec();
  }

  findAllByBClientCode(BClientCode: string) {
    return this.blogsModel.find({ BClientCode }).exec();
  }

  findAllActive() {
    return this.blogsModel.find({ Active: true }).exec();
  }

  findById(id: string) {
    return this.blogsModel.findById(id).exec();
  }

  updateById(_id: string, changes: UpdateBlogDto) {
    return this.blogsModel
      .findByIdAndUpdate(_id, { $set: changes }, { new: true })
      .exec();
  }

  removeById(id: string) {
    return this.blogsModel.findByIdAndDelete(id).exec();
  }
}
